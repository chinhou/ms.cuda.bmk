#include <stdio.h>

#include "repeat.h"


/* These kernels are empty. They will be replaced by compiled cubin versions
   from regfile.real_cubin */
extern "C" {
__global__ void kempty_4 (unsigned int* out) {}
__global__ void kempty_8 (unsigned int* out) {}
__global__ void kempty_12 (unsigned int* out) {}
__global__ void kempty_16 (unsigned int* out) {}
__global__ void kempty_20 (unsigned int* out) {}
__global__ void kempty_24 (unsigned int* out) {}
__global__ void kempty_28 (unsigned int* out) {}
__global__ void kempty_32 (unsigned int* out) {}
__global__ void kempty_36 (unsigned int* out) {}
__global__ void kempty_40 (unsigned int* out) {}
__global__ void kempty_44 (unsigned int* out) {}
__global__ void kempty_48 (unsigned int* out) {}
__global__ void kempty_52 (unsigned int* out) {}
__global__ void kempty_56 (unsigned int* out) {}
__global__ void kempty_60 (unsigned int* out) {}
__global__ void kempty_64 (unsigned int* out) {}
__global__ void kempty_68 (unsigned int* out) {}
__global__ void kempty_72 (unsigned int* out) {}
__global__ void kempty_76 (unsigned int* out) {}
__global__ void kempty_80 (unsigned int* out) {}
__global__ void kempty_84 (unsigned int* out) {}
__global__ void kempty_88 (unsigned int* out) {}
__global__ void kempty_92 (unsigned int* out) {}
__global__ void kempty_96 (unsigned int* out) {}
__global__ void kempty_100 (unsigned int* out) {}
__global__ void kempty_104 (unsigned int* out) {}
__global__ void kempty_108 (unsigned int* out) {}
__global__ void kempty_112 (unsigned int* out) {}
__global__ void kempty_116 (unsigned int* out) {}
__global__ void kempty_120 (unsigned int* out) {}
__global__ void kempty_124 (unsigned int* out) {}
__global__ void kempty_128 (unsigned int* out) {}
__global__ void kempty_132 (unsigned int* out) {}
__global__ void kempty_136(unsigned int* out) {}
__global__ void kempty_140(unsigned int* out) {}
__global__ void kempty_144(unsigned int* out) {}
__global__ void kempty_148(unsigned int* out) {}
__global__ void kempty_152(unsigned int* out) {}
__global__ void kempty_156(unsigned int* out) {}
__global__ void kempty_160(unsigned int* out) {}
__global__ void kempty_164(unsigned int* out) {}
__global__ void kempty_168(unsigned int* out) {}
__global__ void kempty_172(unsigned int* out) {}
__global__ void kempty_176(unsigned int* out) {}
__global__ void kempty_180(unsigned int* out) {}
__global__ void kempty_184(unsigned int* out) {}
__global__ void kempty_188(unsigned int* out) {}
__global__ void kempty_192(unsigned int* out) {}
__global__ void kempty_196(unsigned int* out) {}
__global__ void kempty_200(unsigned int* out) {}
__global__ void kempty_204(unsigned int* out) {}
__global__ void kempty_208(unsigned int* out) {}
__global__ void kempty_212(unsigned int* out) {}
__global__ void kempty_216(unsigned int* out) {}
__global__ void kempty_220(unsigned int* out) {}
__global__ void kempty_224(unsigned int* out) {}
__global__ void kempty_228(unsigned int* out) {}
__global__ void kempty_232(unsigned int* out) {}
__global__ void kempty_236(unsigned int* out) {}
__global__ void kempty_240(unsigned int* out) {}
__global__ void kempty_244(unsigned int* out) {}
__global__ void kempty_248(unsigned int* out) {}
__global__ void kempty_252(unsigned int* out) {}
__global__ void kempty_256(unsigned int* out) {}
__global__ void kempty_260(unsigned int* out) {}
__global__ void kempty_264(unsigned int* out) {}
__global__ void kempty_268(unsigned int* out) {}
__global__ void kempty_272(unsigned int* out) {}
__global__ void kempty_276(unsigned int* out) {}
__global__ void kempty_280(unsigned int* out) {}
__global__ void kempty_284(unsigned int* out) {}
__global__ void kempty_288(unsigned int* out) {}
__global__ void kempty_292(unsigned int* out) {}
__global__ void kempty_296(unsigned int* out) {}
__global__ void kempty_300(unsigned int* out) {}
__global__ void kempty_304(unsigned int* out) {}
__global__ void kempty_308(unsigned int* out) {}
__global__ void kempty_312(unsigned int* out) {}
__global__ void kempty_316(unsigned int* out) {}
__global__ void kempty_320(unsigned int* out) {}
__global__ void kempty_324(unsigned int* out) {}
__global__ void kempty_328(unsigned int* out) {}
__global__ void kempty_332(unsigned int* out) {}
__global__ void kempty_336(unsigned int* out) {}
__global__ void kempty_340(unsigned int* out) {}
__global__ void kempty_344(unsigned int* out) {}
__global__ void kempty_348(unsigned int* out) {}
__global__ void kempty_352(unsigned int* out) {}
__global__ void kempty_356(unsigned int* out) {}
__global__ void kempty_360(unsigned int* out) {}
__global__ void kempty_364(unsigned int* out) {}
__global__ void kempty_368(unsigned int* out) {}
__global__ void kempty_372(unsigned int* out) {}
__global__ void kempty_376(unsigned int* out) {}
__global__ void kempty_380(unsigned int* out) {}
__global__ void kempty_384(unsigned int* out) {}
__global__ void kempty_388(unsigned int* out) {}
__global__ void kempty_392(unsigned int* out) {}
__global__ void kempty_396(unsigned int* out) {}

}


#define REGTEST1(RPT)	\
	for (Db.x = 0, max_success = -1; Db.x <= 516; Db.x+=1)	\
	{														\
		try{ kempty_##RPT <<<Dg, Db>>>(d_out);} catch (...)	{continue;}		\
		if (cudaGetLastError() == cudaSuccess) { max_success = Db.x;}			\
	}	\
	printf ("  [%3d x %3d = %5d]\n", max_success, RPT, max_success*RPT);	\

void measure_regfile()
{
	unsigned int *d_out;			// Unused memory for storing output
	
   	dim3 Db = dim3(1);
   	dim3 Dg = dim3(1,1,1);
	
	if (cudaSuccess != cudaMalloc((void**)&d_out, 4))
	{
		printf ("cudaMalloc failed %s:%d\n", __FILE__, __LINE__);
		return;
	}
	
	printf ("\n");
    	printf ("Running register file capacity test...\n");
	cudaGetLastError();		// Clear previous error code, if any
	
	printf ("  [Max threads x regs/thread = registers used] before kernel spawn failure.\n");
	int max_success;
	REGTEST1(4);
	REGTEST1(8);
	REGTEST1(12);
	REGTEST1(16);
	REGTEST1(20);
	REGTEST1(24);
	REGTEST1(28);
	REGTEST1(32);
	REGTEST1(36);
	REGTEST1(40);
	REGTEST1(44);
	REGTEST1(48);
	REGTEST1(52);
	REGTEST1(56);
	REGTEST1(60);
	REGTEST1(64);
	REGTEST1(68);
	REGTEST1(72);
	REGTEST1(76);
	REGTEST1(80);
	REGTEST1(84);
	REGTEST1(88);
	REGTEST1(92);
	REGTEST1(96);
	REGTEST1(100);
	REGTEST1(104);
	REGTEST1(108);
	REGTEST1(112);
	REGTEST1(116);
	REGTEST1(120);
	REGTEST1(124);
	REGTEST1(128);
	REGTEST1(132);
	REGTEST1(136);
	REGTEST1(140);
	REGTEST1(144);
	REGTEST1(148);
	REGTEST1(152);
	REGTEST1(156);
	REGTEST1(160);
	REGTEST1(164);
	REGTEST1(168);
	REGTEST1(172);
	REGTEST1(176);
	REGTEST1(180);
	REGTEST1(184);
	REGTEST1(188);
	REGTEST1(192);
	REGTEST1(196);
	REGTEST1(200);
	REGTEST1(204);
	REGTEST1(208);
	REGTEST1(212);
	REGTEST1(216);
	REGTEST1(220);
	REGTEST1(224);
	REGTEST1(228);
	REGTEST1(232);
	REGTEST1(236);
	REGTEST1(240);
	REGTEST1(244);
	REGTEST1(248);
	REGTEST1(252);
	REGTEST1(256);
	REGTEST1(260);
	REGTEST1(264);
	REGTEST1(268);
	REGTEST1(272);
	REGTEST1(276);
	REGTEST1(280);
	REGTEST1(284);
	REGTEST1(288);
	REGTEST1(292);
	REGTEST1(296);
	REGTEST1(300);
	REGTEST1(304);
	REGTEST1(308);
	REGTEST1(312);
	REGTEST1(316);
	REGTEST1(320);
	REGTEST1(324);
	REGTEST1(328);
	REGTEST1(332);
	REGTEST1(336);
	REGTEST1(340);
	REGTEST1(344);
	REGTEST1(348);
	REGTEST1(352);
	REGTEST1(356);
	REGTEST1(360);
	REGTEST1(364);
	REGTEST1(368);
	REGTEST1(372);
	REGTEST1(376);
	REGTEST1(380);
	REGTEST1(384);
	REGTEST1(388);
	REGTEST1(392);
	REGTEST1(396);


	printf ("\n");
	
	cudaFree(d_out);
}

#ifdef _WIN32
int main(int argc, char** argv) {
	measure_regfile();
}
#endif

