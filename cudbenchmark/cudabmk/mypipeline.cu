//#include <stdio.h>
// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#include "repeat.h"
#include "instructions.h"

#include "device_functions.h"

/* This is alternating MAD and MUL to measure the dual issue. */
__global__ void KMAD_MUL(unsigned int *ts, unsigned int* out, float p1, float  p2, int its)
{
	float t1 = p1;
	float t2 = p2;
	float t3 = p1 + p2;
	float t4 = p1*p2;
	unsigned int start_time = 0, stop_time = 0;

	for (int i = 0; i<its; i++)
	{
		__syncthreads();
		start_time = clock();
		repeat128(t1 += t1*t3; t2 *= t4;)
			stop_time = clock();
	}

	out[0] = (unsigned int)(t1 + t2 + t3 + t4);
	ts[(blockIdx.x*blockDim.x + threadIdx.x) * 2] = start_time;
	ts[(blockIdx.x*blockDim.x + threadIdx.x) * 2 + 1] = stop_time;
}


template<typename FUNC>
void MEASURE_LATENCY(FUNC func) {
	dim3 Db = dim3(1, 1, 1);
	dim3 Dg = dim3(1, 1);
	Db.x = 1;
	FUNC << <Dg, Db >> > (d_ts, d_out, 4, 6, 2);
	cudaThreadSynchronize();
	cudaError_t error;
	printf("  %s \tlatency:    \t", #FUNC);
	if ((error = cudaGetLastError()) != cudaSuccess) {
		printf("  failed. %s\n\n", cudaGetErrorString(error));
		break;
	}
	cudaMemcpy(ts, d_ts, sizeof(ts), cudaMemcpyDeviceToHost);
	cudaThreadSynchronize();
	printf("%u clk (%.3f clk/warp)\n", ts[1] - ts[0], ((double)(ts[1] - ts[0]) / kernel_ops));
}


void measure_pipeline()
{

	const int kernel_ops = 256;		// kernels have this many operations
	unsigned int ts[2 * 1024];		// ts, output from kernel. Two elements used per thread.
	unsigned int *d_ts;
	unsigned int *d_out;			// Unused memory for storing output




	// Allocate device array.
	if (cudaSuccess != cudaMalloc((void**)&d_ts, sizeof(ts)))
	{
		printf("cudaMalloc failed %s:%d\n", __FILE__, __LINE__);
		return;
	}
	if (cudaSuccess != cudaMalloc((void**)&d_out, 4))
	{
		printf("cudaMalloc failed %s:%d\n", __FILE__, __LINE__);
		return;
	}

	cudaGetLastError();
	fprintf(stderr, "Running pipeline tests...\n\n");



	printf("Measuring latency of syncthreads with multiple warps running:\n");
	for (int i = 1; i <= 512 / 32; i++)
	{
		printf("%d warps: ", i);
		MEASURE_LATENCY2(K_SYNC_UINT_DEP128, i * 32);
	}

	cudaFree(d_ts);
	cudaFree(d_out);

}


#ifdef _WIN32
int main(int argc, char** argv) {
	measure_pipeline();
	return 0;
}
#endif // _WIN32