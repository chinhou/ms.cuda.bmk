#include <stdio.h>

#include "repeat.h"

__global__ void shared_latency(int * vec, int array_length, int iterations, int idx, unsigned long long * duration) {

	unsigned int start_time, end_time;
	int i, k;
	unsigned int j = 0;
	unsigned long long sum_time;

	vec[array_length - 1] = 0;
	sum_time = 0;
	duration[0] = 0;
	int x0 = vec[0]; int x1 = vec[1]; int x2 = vec[2]; int x3 = vec[3];
	int x4 = vec[4]; int x5 = vec[5]; int x6 = vec[6]; int x7 = vec[7];
	int x8 = vec[8]; int x9 = vec[9]; int x10 = vec[10]; int x11 = vec[11];
	int x12 = vec[12]; int x13 = vec[13]; int x14 = vec[14]; int x15 = vec[15];

	// sdata[] is used to hold the data in shared memory. Dynamically allocated at launch time.
	extern __shared__ int sdata[];

	j = 0;
	for (k = 0; k <= iterations; k++) {
		if (k == 1) {
			sum_time = 0;
		}

		start_time = clock();
		sdata[x0]  = x0;  sdata[x1]  = x1;  sdata[x2] = x2;   sdata[x3] = x3;
		sdata[x4]  = x4;  sdata[x5]  = x5;  sdata[x6] = x6;   sdata[x7] = x7;
		sdata[x8]  = x8;  sdata[x9]  = x9;  sdata[x10] = x10; sdata[x11] = x11;
		sdata[x12] = x12; sdata[x13] = x13; sdata[x14] = x14; sdata[x15] = x15;
		end_time = clock();

		sum_time += (end_time - start_time);
		int t = x0;
		x0 = x1; x1 = x2; x2 = x3; x3 = x4; 
		x4 = x5; x5 = x6; x6 = x7; x7 = x8;
		x8 = x9; x9 = x10; x10 = x11; x11 = x12; 
		x12 = x13; x13 = x14; x14 = x15; x15 = t;
	}

	if (idx == threadIdx.x)
		vec[array_length - 1] = sdata[x0];
	duration[0] = sum_time;

}

// Shared memory array size is N-2. Last two elements are used as dummy variables.
void parametric_measure_shared(int N, int iterations, int stride) {

	int i;
	unsigned int * h_a;
	unsigned int * d_a;

	unsigned long long * duration;
	unsigned long long * latency;

	cudaError_t error_id;

	/* allocate array on CPU */
	h_a = (unsigned int *)malloc(sizeof(unsigned int) * N);
	latency = (unsigned long long *)malloc(2 * sizeof(unsigned long long));

	/* initialize array elements on CPU */
	for (i = 0; i < N - 2; i++) {
		h_a[i] = (i + stride) % (N - 2);
	}
	h_a[N - 2] = 0;
	h_a[N - 1] = 0;


	/* allocate arrays on GPU */
	cudaMalloc((void **)&d_a, sizeof(unsigned int) * N);
	cudaMalloc((void **)&duration, 2 * sizeof(unsigned long long));

	cudaThreadSynchronize();
	error_id = cudaGetLastError();
	if (error_id != cudaSuccess) {
		printf("Error 1 is %s\n", cudaGetErrorString(error_id));
	}

	/* copy array elements from CPU to GPU */
	cudaMemcpy((void *)d_a, (void *)h_a, sizeof(unsigned int) * N, cudaMemcpyHostToDevice);
	cudaMemcpy((void *)duration, (void *)latency, 2 * sizeof(unsigned long long), cudaMemcpyHostToDevice);

	cudaThreadSynchronize();

	error_id = cudaGetLastError();
	if (error_id != cudaSuccess) {
		printf("Error 2 is %s\n", cudaGetErrorString(error_id));
	}

	/* launch kernel*/
	dim3 Db = dim3(1);
	dim3 Dg = dim3(1, 1, 1);

	//printf("Launch kernel with parameters: %d, N: %d, stride: %d\n", iterations, N, stride); 
	int sharedMemSize = sizeof(unsigned int) * N;

	shared_latency << <Dg, Db, sharedMemSize >> >(d_a, N, iterations, duration);

	cudaThreadSynchronize();

	error_id = cudaGetLastError();
	if (error_id != cudaSuccess) {
		printf("Error 3 is %s\n", cudaGetErrorString(error_id));
	}

	/* copy results from GPU to CPU */
	cudaThreadSynchronize();

	cudaMemcpy((void *)h_a, (void *)d_a, sizeof(unsigned int) * N, cudaMemcpyDeviceToHost);
	cudaMemcpy((void *)latency, (void *)duration, 2 * sizeof(unsigned long long), cudaMemcpyDeviceToHost);

	cudaThreadSynchronize();

	/* print results*/


	printf("  %d, %f\n", stride, (double)(latency[0] / (256.0*iterations)));


	/* free memory on GPU */
	cudaFree(d_a);
	cudaFree(duration);
	cudaThreadSynchronize();

	/*free memory on CPU */
	free(h_a);
	free(latency);


}


int main() {

	int N, stride;

	// initialize upper bounds here
	int stride_upper_bound = 1024;

	printf("Shared memory latency for varying stride.\n");
	printf("stride (bytes), latency (clocks)\n");

	N = 256;
	stride_upper_bound = N;
	for (stride = 1; stride <= stride_upper_bound; stride += 1) {
		parametric_measure_shared(N + 2, 10, stride);
	}

	return 0;

}
